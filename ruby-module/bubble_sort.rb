def bubble_sort(arr)
  flag = true;
  while flag do
    flag = false
    (0...arr.length - 1).find_all do |i|
      if arr[i] > arr[i+1]
        tmp = arr[i]
        arr[i] = arr[i+1]
        arr[i+1] = tmp
        flag = true;
      end
    end
  end
  arr
end

puts bubble_sort([4,3,78,2,0,2])
