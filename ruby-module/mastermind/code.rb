class Code
  @@guess_values = [1, 2, 3, 4, 5, 6]
  # attr_reader :code
  def initialize()
    @code = @@guess_values.sample(4)
  end

  def guess(attempt)
    puts "Attempt #{attempt}"
    ret_arr = Array.new(4) {0}
    if attempt == @code
      return [2, 2, 2, 2]
    end

    attempt.each_with_index do |num, i|

      unless @@guess_values.include?(num)
        return nil
      end

      if @code[i] == num
        # puts "Number #{num} in right place"
        ret_arr[i] = 2
        next
      end
      if @code.include?(num)
        # "Number #{num} found in code"
        ret_arr[i] = 1
        next
      end
    end
    ret_arr.sort
  end
end
