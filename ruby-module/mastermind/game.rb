require "./code.rb"

puts " **************"
puts "*  Mastermind  *"
puts " **************"
puts "\n\n"

my_code= Code.new
guesses = 0;
found = false

while guesses < 12 do
  puts "Guess ##{guesses + 1} / 12"
  puts "Enter a four digit guess"
  guess = gets.chomp.split("").map { |n| n.to_i }

  unless guess.length == 4
    puts "Enter 4 digits 0-6"
    next
  end

  result = my_code.guess(guess)

  unless result == nil
    result.each do |i|
      case i
      when 0
        print "\e[31m" + "▓"
      when 1
        print "\e[33m" + "▓"
      when 2
        print "\e[32m" + "▓"
      end
    end
    print "\n\e[0m"
    if result == [2, 2, 2, 2]
      found = true
      break;
    end
    guesses += 1

  else
    puts "Invalid guess (digits 1-6 only)"
    next
  end

end

if found
  puts "\e[32m*** Congratulations! You cracked the code! ***"
else
  puts "\e[31mBetter luck next time..."
end
