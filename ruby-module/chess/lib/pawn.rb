require_relative "./piece.rb"
# require_relative "./helper.rb"

class Pawn < Piece

  def initialize(player, board, x, y)
    super(player, board, x, y)
    @TRANSFORMS = [
      [0, 2], [0, 1],
      [-1, 1], [1, 1]
    ]
    if @color == 0
      @TRANSFORMS = @TRANSFORMS.map { |t| [t[0], t[1] * -1] }
    end

    @symbol = (@color == 0) ? white_pawn : black_pawn
  end

  def legal_moves
    @moves = []
    for transform in @TRANSFORMS
      x = @x + transform[0]
      y = @y + transform[1]
      next unless (x.between?(0, 7) and y.between?(0, 7))
      square = @board.query(x, y)
      unless transform[0] == 0
        if square && square.color != @color
          @moves << [x, y]
        else
          next
        end
      else
        if square
          next
        else
          @moves << [x, y]
        end
      end
    end
    @moves
  end

  def controlled_squares
    control = []
    for t in @TRANSFORMS[2..3]
      x = @x + t[0]
      y = @y + t[1]
      if (x.between?(0, 7) and y.between?(0, 7))
        control << [x, y]
      end
    end
    control
  end
end
