require_relative "./helper.rb"
require_relative "./king.rb"
require_relative "./queen.rb"
require_relative "./rook.rb"
require_relative "./bishop.rb"
require_relative "./knight.rb"
require_relative "./pawn.rb"

class Player
  include Helper

  attr_reader :legal_moves, :pieces, :color
  attr_accessor :checked

  def initialize(color)
    @checked = false
    @color = color
    @pieces = []
    @legal_moves = []

    @pawns = []
    @bishops = []
    @knights = []
    @rooks = []
    @queen = nil
    @king = nil
    @board = nil

    @check = false
  end

  def generate_legal_moves
    @legal_moves = []
    for piece in @pieces
      piece.legal_moves
      @legal_moves << [piece.coords, piece.moves]
    end
    @legal_moves
  end

  def controlled_squares
    sq = []
    for piece in @pieces
      sq.concat piece.controlled_squares
    end
    sq
  end

  def register_board(board)
    @board = board
  end

  def add_piece(piece)
    @pieces << piece

    case piece.class.name
    when "Pawn"
      @pawns << piece
    when "Knight"
      @knights << piece
    when "Bishop"
      @bishops << piece
    when "Rook"
      @rooks << piece
    when "Queen"
      @queen = piece
    when "King"
      @king = piece
    end

    if piece.class.name == "King"
      puts @king.coords
    end
  end

  def turn
    if @check
      puts "IN CHECK!"
      check_turn()
      return
    end
    self.generate_legal_moves

    loop do
      move = get_move

      start_xy = get_xy_from_algebraic(move[0])
      end_xy = get_xy_from_algebraic(move[1])
      piece_to_move = @board.query(start_xy[0], start_xy[1])

      unless piece_to_move
        puts "No piece on that square"
        next
      end

      unless piece_to_move.color == @color
        puts "Cannot move that piece."
        next
      end

      unless piece_to_move.moves.include?(end_xy)
        puts "Cannot move that piece to that square"
        next
      end

      if (@board.query(end_xy[0], end_xy[1]))
        @board.capture(start_xy, end_xy)
      else
        @board.move(start_xy, end_xy)
      end
      break
    end

    @board.check_for_checks(@color)
  end

  def check_turn
    self.generate_legal_moves

    #How many pieces are attacking the king?
    enemy = (color == 0) ? 1 : 0
    enemy = @board.players[enemy]
    pieces_attacking = enemy.controlled_squares.count(king_square)

    #If only one piece?
    if pieces_attacking == 1

      #Get the square of the attacking piece
      enemy_piece = nil
      enemy.generate_legal_moves
      for piece in enemy.legal_moves
        for i in piece[1]
          if i == king_square
            enemy_piece = piece[0]
            break
          end
        end
        if enemy_piece
          break
        end
      end
      if enemy_piece.nil?
        return nil
      end

      #We can capture the attacking piece
      capture_moves = []
      for piece in @legal_moves
        for i in piece[1]
          if i == enemy_piece
            if piece[0] == king_square
              unless enemy.is_controlled?(i)
                capture_moves << [piece[0], enemy_piece]
              end
              next
            end
            capture_moves << [piece[0], enemy_piece]
          end
        end
      end

      #We can maybe block that piece
      block_moves = []
      enemy_piece = @board.query(enemy_piece[0], enemy_piece[1])
      if enemy_piece.can_be_blocked?
        squares_to_block_on = enemy_piece.line_of_sight(king_square[0], king_square[1])
        for square in squares_to_block_on
          for piece in @legal_moves
            for i in piece[1]
              if i == square
                unless piece[0] == king_square
                  block_moves << [piece[0], square]
                end
              end
            end
          end
        end
      end
    end

    #No matter how many checks are given, the king is always able to move.
    king_moves = @king.legal_moves
    enemy_squares = enemy.controlled_squares
    for sq in enemy_squares
      if king_moves.include?(sq)
        king_moves.delete(sq)
      end
    end

    all_legal_moves = capture_moves.concat block_moves
    all_legal_moves.concat king_moves

    for move in all_legal_moves
      p move
    end

    if all_legal_moves.length == 0
      puts "No Legal Moves."
      @board.resign(@color)
      return
    end

    loop do
      move = get_move
      from = get_xy_from_algebraic(move[0])
      to = get_xy_from_algebraic(move[1])
      if all_legal_moves.include?([from, to])
        if @board.query(to[0], to[1])
          @board.capture(from, to)
        else
          @board.move(from, to)
        end
      else
        puts "Invalid move! You are in check!"
        next
      end
      break
    end

    @check = false
    @board.check_for_checks(@color)


  end

  def king_square
    @king.coords
  end

  def get_move
    puts "Enter the move you would like to play (startsquare endsquare)"
    move = gets.chomp.split
    until (move[0].match(/^[a-hA-H][1-8]$/) and move[1].match(/^[a-hA-H][1-8]$/))
      puts "Invalid move format"
      puts "#{move}"
      puts "Enter a new move:"
      move = gets.chomp.split
    end
    move
  end

  def check
    @check = true
  end

  def is_controlled?(coord)
    return controlled_squares.include?(coord)
  end

  def remove_piece(piece)
    @pieces.delete(piece)
  end

  def get_xy_from_algebraic(str)
    str = str.split("")
    x = "abcdefgh"
      .split("")
      .find_index(str[0].downcase)
    y = (str[1].to_i - 8).abs
    [x, y]
  end

  def checked?
    return @check
  end
end
