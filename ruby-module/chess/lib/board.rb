require_relative "./helper.rb"
require_relative "./king.rb"
require_relative "./queen.rb"
require_relative "./rook.rb"
require_relative "./bishop.rb"
require_relative "./knight.rb"
require_relative "./pawn.rb"
require_relative "./player.rb"

class Board

  attr_reader :current_turn, :turns, :board, :moves, :players, :finished
  attr_writer :board

  def initialize(p_one, p_two)
    @board = Array.new(8) { Array.new(8) {nil} }
    @players = [p_one, p_two]
    @current_turn = @players[0]

    @players[0] = p_one
    @players[1] = p_two

    p_one.register_board(self)
    p_two.register_board(self)

    @turn = 0
    @turns = 0
    @finished = false
  end

  def start
    #White pawns
    for i in (0..7) do
      @board[6][i] = Pawn.new(@players[0], self, i, 6)
    end

    #Black pawns
    for i in (0..7) do
      @board[1][i] = Pawn.new(@players[1], self, i, 1)
    end

    #Bishops
    @board[7][2] = Bishop.new(@players[0], self, 2, 7)
    @board[7][5] = Bishop.new(@players[0], self, 5, 7)
    @board[0][2] = Bishop.new(@players[1], self, 2, 0)
    @board[0][5] = Bishop.new(@players[1], self, 5, 0)

    #Knights
    @board[7][1] = Knight.new(@players[0], self, 1, 7)
    @board[7][6] = Knight.new(@players[0], self, 6, 7)
    @board[0][1] = Knight.new(@players[1], self, 1, 0)
    @board[0][6] = Knight.new(@players[1], self, 6, 0)

    #Rooks
    @board[7][0] = Rook.new(@players[0], self, 0, 7)
    @board[7][7] = Rook.new(@players[0], self, 7, 7)
    @board[0][0] = Rook.new(@players[1], self, 7, 7)
    @board[0][7] = Rook.new(@players[1], self, 7, 0)

    #Queens
    @board[7][3] = Queen.new(@players[0], self, 3, 7)
    @board[0][3] = Queen.new(@players[1], self, 3, 0)

    #Kings
    @board[7][4] = King.new(@players[0], self, 4, 7)
    @board[0][4] = King.new(@players[1], self, 4, 0)

  end

  def print_white
    puts ""
    for i in (0..7)
      print "#{(i - 8).abs} "
      for j in (0..7)
        if @board[i][j]
          print "#{@board[i][j].symbol} "
        else
          print "  "
        end
      end
      print "\n"
    end
    puts "  A B C D E F G H"
  end

  def add_check(color)
    @players[color].check
  end

  def check_for_checks(color)
    controlled = @players[color].controlled_squares
    # p controlled
    opposition = (color == 0) ? 1 : 0
    opposed_king = @players[opposition].king_square

    if controlled.any? { |square| square == opposed_king }
      add_check(opposition)
    end
  end

  def print_black
    puts ""
    for i in (0..7).reverse_each
      print "#{(i - 8).abs} "
      for j in (0..7).reverse_each
        if @board[i][j]
          print "#{@board[i][j].symbol} "
        else
          print "  "
        end
      end
      print "\n"
    end
    puts "  H G F E D C B A"
  end

  def print_board
    (@current_turn == @players[0]) ? print_white : print_black
  end

  def query(x, y)
    @board[y][x]
  end

  def resign(color)
    puts (color == 0) ? "# 0 - 1" : "# 1 - 0"
    @finished = true
  end

  # Can be merged with move() below
  def capture(start_xy, end_xy)
    capturing = query(start_xy[0], start_xy[1])
    captured = query(end_xy[0], end_xy[1])

    captured.player.remove_piece(captured)
    capturing.update_xy(end_xy[0], end_xy[1])
    @board[end_xy[1]][end_xy[0]] = capturing
    @board[start_xy[1]][start_xy[0]] = nil
    @current_turn = (@current_turn == @players[0]) ? @players[1] : @players[0]
  end

  def move(start_xy, end_xy)
    piece = query(start_xy[0], start_xy[1])
    piece.update_xy(end_xy[0], end_xy[1])
    @board[end_xy[1]][end_xy[0]] = piece
    @board[start_xy[1]][start_xy[0]] = nil
    @current_turn = (@current_turn == @players[0]) ? @players[1] : @players[0]
  end

end
