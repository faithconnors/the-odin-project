require_relative "./piece.rb"

class King < Piece
  attr_reader :x, :y
  def initialize(player, board, x, y)
    super(player, board, x, y)
    @TRANSFORMS = [
      [1, 0], [-1, 0],
      [0, 1], [0, -1],
      [1, 1], [1, -1],
      [-1, 1], [-1, -1]
    ]
    @symbol = (player.color == 0) ? white_king : black_king
    puts "Setting king square"
    puts "King square: [#{player.king_square}]"
  end

  def update_xy(x, y)
    @x = x
    @y = y
    @player.king_square = [x, y]
  end
end
