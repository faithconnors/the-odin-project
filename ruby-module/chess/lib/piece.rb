require_relative "./helper.rb"

class Piece
include Helper

  attr_reader :moves, :symbol, :color, :player
  @TRANSFORMS = []

  def initialize(player, board, x, y)
    @x = x
    @y = y
    @color = player.color
    @board = board
    player.add_piece(self)
    @player = player
    @symbol = ""
    @moves = []
    @controlled = []
    @blockable = false
  end

  def algebraic
    file = "ABCDEFGH"[@x]
    rank = (@y - 8).abs
    "#{file}#{rank}"
  end

  def coords
    [@x, @y]
  end

  def legal_moves
    @moves = []
    for transform in @TRANSFORMS
      x = @x + transform[0]
      y = @y + transform[1]
      next unless (x.between?(0, 7) and y.between?(0, 7))
      check_square(x, y, transform)
    end
    @moves
  end

  def controlled_squares
    @controlled = []
    for transform in @TRANSFORMS
      x = @x + transform[0]
      y = @y + transform[1]
      next unless (x.between?(0, 7) and y.between?(0, 7))
      check_control(x, y, transform)
    end
    @controlled
  end

  def can_be_blocked?
    @blockable
  end

  def line_of_sight(x, y)
    squares = []
    diff_x = x - @x
    diff_y = y - @y
    if (diff_x != 0)
      diff_x /= diff_x.abs
    end

    if (diff_y != 0)
      diff_y /= diff_y.abs
    end

    t = [diff_x, diff_y]

    t_x = @x + t[0]
    t_y = @y + t[1]

    while (t_x != x and t_y != y)
      squares << [t_x, t_y]
      t_x += t[0]
      t_y += + t[1]
    end

    squares
  end

  def update_xy(x, y)
    @x = x
    @y = y
  end

  private

  def check_square(x, y, transform)
    current_square = @board.query(x, y)
    if current_square
      if current_square.color == @color
        return
      else
        @moves << [x, y]
        return
      end
    else
      @moves << [x, y]
    end
  end

  def check_control(x, y, transform)
    current_square = @board.query(x, y)
    if current_square
      @controlled << [x, y]
      return true
    else
      @controlled << [x, y]
    end
    return false
  end

end
