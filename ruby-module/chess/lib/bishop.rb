require_relative "./piece.rb"
# require_relative "./helper.rb"

class Bishop < Piece
# include Helper

  def initialize(player, board, x, y)
    super(player, board, x, y)
    @TRANSFORMS = [
      [1, 1], [-1, 1],
      [-1, -1], [1, -1]
    ]
    @symbol = (@color == 0) ? white_bishop : black_bishop
    @blockable = true
  end

  private

  def check_square(x, y, transform)
    super(x, y, transform)
    x += transform[0]
    y += transform[1]
    return unless (x.between?(0, 7) and y.between?(0, 7))
    check_square(x, y, transform)
  end

  def check_control(x, y, transform)
    res = super(x, y, transform)
    if res
      return
    end
    x += transform[0]
    y += transform[1]
    return unless (x.between?(0, 7) and y.between?(0, 7))
    check_control(x, y, transform)
  end
end
