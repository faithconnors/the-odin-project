require_relative "./piece.rb"

class Knight < Piece
  def initialize(player, board, x, y)
    super(player, board, x, y)
    @TRANSFORMS = [
      [1, 2], [-1, 2], [-1, -2], [1, -2],
      [2, 1], [-2, 1], [-2, -1], [2, -1]
    ]
    @symbol = (player.color == 0) ? white_knight : black_knight
  end
end
