require "./lib/calculator"

describe Calculator do
  describe "#add" do
    it "returns the sum of two numbers" do
      calculator = Calculator.new
      expect(calculator.add(5, 2)).to eql(7)
    end

    it "returns the sum of more than two numbers" do
      calculator = Calculator.new
      expect(calculator.add(2, 5, 7)).to eql(14)
    end
  end

  describe "#multiply" do
    it "returns the product of two numbers" do
      calculator = Calculator.new
      expect(calculator.multiply(3, 4)).to eql(12)
    end

    it "returns the number if only one is provided" do
      calculator = Calculator.new
      expect(calculator.multiply(3)).to eql(3)
    end

    it "returns the product of n numbers" do
      calculator = Calculator.new
      expect(calculator.multiply(3, 4, 5)).to eql(60)
    end
  end

  describe "#subtract" do
    it "returns the difference between two numbers" do
      calculator = Calculator.new
      expect(calculator.subtract(10, 5)).to eql(5)
    end

    it "returns the number if only one number is provided" do
      calculator = Calculator.new
      expect(calculator.subtract(5)).to eql(5)
    end

    it "returns the difference from repeated subtraction" do
      calculator = Calculator.new
      expect(calculator.subtract(10, 5, 3)).to eql(2)
    end
  end

  describe "#divide" do
    it "returns the quotient of two numbers" do
      calculator = Calculator.new
      expect(calculator.divide(10, 5)).to eql(2.0)
    end

    it "returns one argument if only one passed" do
      calculator = Calculator.new
      expect(calculator.divide(5)).to eql(5.0)
    end

    it "performs repeated division for n>2 arguments" do
      calculator = Calculator.new
      expect(calculator.divide(10, 5, 2)).to eql(1.0)
    end
  end
end
