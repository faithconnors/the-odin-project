class Calculator
  def add(*args)
    sum = 0
    for n in args
      sum += n
    end
    sum
  end

  def multiply(*args)
    product = 1
    for n in args
      product *= n
    end
    product
  end

  def subtract(n, *args)
    for i in args
      n -= i
    end
    n
  end

  def divide(n, *args)
    n = n.to_f
    for i in args
      n /= i
    end
    n
  end
end
