require "./lib/helper.rb"

class Board
  include Helper

  attr_reader :turn, :board

  TRANSFORMS = [
    [-1, 1], [0, 1],
    [ 1, 1], [1, 0]
  ]

  def initialize
    @board = Array.new(7) { Array.new(7) {nil} }
    @turn = red_circle
    @moves = []
  end

  def query(x, y)
    return nil unless (x.between?(1, 7) and y.between?(1, 7))
    (@board[x - 1][y - 1]) ? @board[x - 1][y - 1] : empty_space
  end

  def move(x)

    return false unless x.between?(1, 7)

    y = @board[x - 1].find_index(nil)
    unless y
      puts "that column is full!"
      return false
    end

    @board[x - 1][y] = @turn
    @turn = (@turn == red_circle) ? blue_circle : red_circle
    @moves << [x, y + 1]
    return true
  end

  def print_board
    y = 6
    x = 0
    while y >= 0 do
      while x < 7 do
        print (@board[x][y]) ? @board[x][y] + " ": empty_space + " "
        x += 1
      end
      print "\n"
      x = 0
      y -= 1
    end
  end

  def won?
    if @moves.length < 7
      return false
    end

    for move in @moves do
      match = self.query(move[0], move[1])
      for transform in TRANSFORMS do
        x = move[0] + transform[0]
        y = move[1] + transform[1]
        next unless (x.between?(1, 7) and y.between?(1, 7))
        result = check_square(x, y, match, transform, 1)
        return result if result
      end
    end
    return empty_space if @moves.length == 49
    return false
  end

  private

  def check_square(x, y, match, transform, depth)
    return match if depth == 4
    return nil unless self.query(x, y) == match
    x += transform[0]
    y += transform[1]
    return check_square(x, y, match, transform, depth + 1)
  end
end
