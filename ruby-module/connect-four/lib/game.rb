require "./lib/helper.rb"
require "./lib/board.rb"

include Helper

def new_game
  return Board.new
end

def play_move(board, n)
  result = board.move(n)
  unless result
    loop do
      puts "Try a different move."
      n = gets.chomp.to_i
      result = board.move(n)
      if result
        break
      end
    end
  end
end

def quit
  nil
end

def menu
  puts "\nEnter your choice: "
  puts "1) Play a new game"
  puts "2) Exit"
  choice = gets.chomp
  loop do
    case choice
    when "1"
      return new_game()
    when "2"
      return quit()
    else
      puts "Enter a valid option"
      choice = gets.chomp
    end
  end
end

puts "******************"
puts "** CONNECT-FOUR **"
puts "******************\n"

board = menu()

while board do
  puts "\n\n\n\n\n\n\n\n\n\n\n\n"
  board.print_board
  puts "\n"
  current_turn = board.turn
  puts "#{current_turn}'s turn:"
  print "Enter a column: "
  col = gets.chomp.to_i
  play_move(board, col)

  if board.won?
    if board.won? == empty_space
      puts "********"
      puts "* DRAW *"
      puts "********"
      board = menu()
      break
    end
    puts "\n\n\n\n\n\n\n\n\n\n\n\n"
    board.print_board
    puts "\n"
    puts "***********"
    puts "* #{current_turn} WINS! *"
    puts "***********"
    board = menu()
  end

end
