module Helper
  def empty_space
    "◯"
  end

  def red_circle
    "\e[31m⬤\e[0m"
  end

  def blue_circle
    "\e[34m⬤\e[0m"
  end
end
