require "./lib/board.rb"
require "./lib/helper.rb"

include Helper

describe "board.rb" do
  describe "#init" do
    it "creates a 7x7 array of nils" do
      board = Board.new
      local_board = Array.new(7) { Array.new(7) {nil} }
      expect(board.board).to eql(local_board)
    end
  end

  describe "#query" do
    it "queries a blank space on the board" do
      board = Board.new
      expect(board.query(1, 1)).to eql(empty_space)
    end

    it "queries a space with a red circle" do
      board = Board.new
      board.move(3)
      expect(board.query(3, 1)).to eql(red_circle)
    end

    it "queries a space with a blue circle" do
      board = Board.new
      board.move(3)
      board.move(4)
      expect(board.query(4, 1)).to eql(blue_circle)
    end

    it "queries a circle that is stacked on another circle" do
      board = Board.new
      board.move(3)
      board.move(3)
      expect(board.query(3, 2)).to eql(blue_circle)
    end
  end

  describe "#won?" do
    it "returns false if the board is empty" do
      board = Board.new
      expect(board.won?).to eql(false)
    end

    it "returns false if there are no wins after a few moves" do
      board = Board.new
      7.times do
        board.move(1)
      end
      board.move(2)
      expect(board.won?).to eql(false)
    end

    it "returns the winner if there is a win on the board" do
      board = Board.new
      for i in (1..3)
        board.move(i)
        board.move(i)
      end
      board.move(4)
      expect(board.won?).to eql(red_circle)
    end

    it "returns the winner for diagonal right win" do
      board = Board.new
      board.move(1)
      board.move(2)
      board.move(2)
      board.move(3)
      board.move(3)
      board.move(4)
      board.move(3)
      board.move(4)
      board.move(4)
      board.move(5)
      board.move(4)
      expect(board.won?).to eql(red_circle)
    end

    it "returns blue for diagonal right win" do
      board = Board.new
      board.move(7)
      board.move(1)
      board.move(2)
      board.move(2)
      board.move(3)
      board.move(3)
      board.move(4)
      board.move(3)
      board.move(4)
      board.move(4)
      board.move(5)
      board.move(4)
      expect(board.won?).to eql(blue_circle)
    end

    it "returns the winner for a winner on the edge" do
      board = Board.new
      for i in (4..6)
        board.move(i)
        board.move(i)
      end
      board.move(7)
      expect(board.won?).to eql(red_circle)
    end

    # Super ugly way of populating a drawn board.
    it "returns an empty circle for a draw" do
      board = Board.new
      3.times do
        board.move(1)
        board.move(2)
      end

      3.times do
        board.move(3)
        board.move(4)
      end
      3.times do
        board.move(5)
        board.move(6)
      end
      3.times do
        board.move(7)
      end

      3.times do
        board.move(1)
        board.move(2)
      end
      3.times do
        board.move(3)
        board.move(4)
      end
      3.times do
        board.move(5)
        board.move(6)
      end
      3.times do
        board.move(7)
      end

      for i in (1..7)
        board.move(i)
      end

      expect(board.won?).to eql(empty_space)
    end
  end
end
