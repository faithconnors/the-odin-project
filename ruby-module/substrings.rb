def substrings(str, dict)
  dict.reduce(Hash.new(0)) do |hash, word|
    word_len = word.length
    if str.downcase.include?(word)
      hash[word] = (0 ... str.length - word_len + 1).find_all {|i| str[i, word_len].downcase == word.downcase }.length
    end
    hash
  end
end
