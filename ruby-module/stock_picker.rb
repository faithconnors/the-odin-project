def stock_picker(arr)
  max = arr[1] - arr[0];
  pair = [0, 1]
  (0...arr.length - 1).find_all do |i|
    (i...arr.length).find_all do |j|
      if arr[j] - arr[i] > max
        max = arr[j] - arr[i]
        pair = [i, j]
      end
    end
  end
  pair
end
