require 'csv'
require 'google/apis/civicinfo_v2'
require 'erb'
require 'date'
require 'time'

def clean_zipcode(zipcode)
  zipcode.to_s.rjust(5, "0")[0..4]
end

def legislators_by_zipcode(zip)
  civic_info = Google::Apis::CivicinfoV2::CivicInfoService.new
  civic_info.key = 'AIzaSyClRzDqDh5MsXwnCWi0kOiiBivP6JsSyBw'

  begin
    civic_info.representative_info_by_address(
      address: zip,
      levels: 'country',
      roles: ['legislatorUpperBody', 'legislatorLowerBody']
    ).officials
    rescue
    "You can find your representatives by visiting www.commoncause.org"
  end
end

def save_thankyou_letter(id, form_letter)
  Dir.mkdir("output") unless Dir.exist?("output")

  filename = "output/thanks_#{id}.html"

  File.open(filename, "w") do |file|
    file.puts form_letter
  end
end

def clean_phone_number(phone)
  digits = ["0","1","2","3","4","5","6","7","8","9"]
  phone = phone.split("").map { |digit| digits.include?(digit) ? digit : "" }.join("")
  if phone.length == 11 and phone[0] == "1"
    return phone.split("").shift(1).join("")
  elsif (phone.length <=> 10) != 0
    return nil
  end
  phone
end

puts "Event Manager Initialized!"

template_letter = File.read("form_letter.erb")
erb_template = ERB.new template_letter

most_common_day = Hash.new(0)
most_common_hour = Hash.new(0)

contents = CSV.open("event_attendees.csv", headers: true, header_converters: :symbol)
contents.each_with_index do |row, index|
  name = row[:first_name]

  date = Date.strptime(row[:regdate].split(" ")[0], "%m/%d/%y")
  time = Time.strptime(row[:regdate].split(" ")[1], "%H:%M")

  most_common_day[date.wday] += 1
  most_common_hour[time.hour] += 1

  phone = clean_phone_number(row[:homephone])

  zip_code = clean_zipcode(row[:zipcode])

  legislators = legislators_by_zipcode(zip_code)

  form_letter = erb_template.result(binding)

  save_thankyou_letter(index, form_letter)

end

most_common_day = most_common_day.reduce(0) do |day, (key, value)|
  if most_common_day[day] <= value
    day = key
  end
  day
end

most_common_hour = most_common_hour.reduce(0) do |hour, (key, value)|
  if most_common_hour[hour] <= value
    hour = key
  end
  hour
end

puts "#{most_common_day}"
puts "#{most_common_hour}"
