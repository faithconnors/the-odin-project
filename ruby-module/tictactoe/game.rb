require "./board.rb"
require "./player.rb"

game_board = Board.new
move = 1
players = [nil, nil]

puts "Enter name of p1:"
players[1] = Player.new("X")

puts "Enter name of p2:"
players[0] = Player.new("O")

until game_board.is_full? do
  game_board.print_board
  mv = players[move % 2].get_move
  until game_board.play_move(mv[0], mv[1]) do
    mv = players[move % 2].get_move
  end

  if move >= 5
    if game_board.check_winner != nil
      break
    end
  end
  move += 1
  puts ""
  puts ""
end

game_board.print_board
winner = game_board.check_winner

if winner
  puts (winner == "X") ? "#{players[1].name} wins!" : "#{players[0].name} wins!"
else
  puts "Draw."
end
