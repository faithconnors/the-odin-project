class Player
  attr_reader :name, :symbol
  def initialize(symbol)
    name = gets.chomp
    while name == "" do
      puts "Enter a valid name:"
      name = gets.chomp
    end
    @name = name
    @symbol = symbol
  end

  public

  def get_move
    puts "Enter move, #{@name}"
    mv = gets.chomp
    until mv.length == 2 do
      puts "Enter a valid move"
      mv = gets.chomp
    end
    [mv, @symbol]
  end

end
