class Board
  attr_reader :state

  @@win_states = [
    [0, 4, 8],
    [2, 4, 6],
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8]
  ]

  def initialize
    @state = Array.new(3) { Array.new(3) { " " } }
  end

  public

  def play_move(move, symbol)

    row_col = move.split("")

    unless row_col[0].ord.between?(97, 99) && row_col[1].ord.between?(49, 51)
      puts "Move is out-of-bounds. (Row then column)"
      return false
    end

    unless @state[row_col[0].ord - 97][row_col[1].ord - 49] == " "
      puts "Space is already populated. Choose a blank space."
      return false
    end

    @state[row_col[0].ord - 97][row_col[1].ord - 49] = symbol
    true
  end

  def print_board
    puts "  1 2 3"
    (0...3).find_all do |i|
      print (i + 97).chr + " "
      (0...3).find_all do |j|
        print (j == 2) ? "#{@state[i][j]}\n" : "#{@state[i][j]}┃"
      end
      puts "  ━╋━╋━" if i != 2
    end
  end

  def check_winner
    linear_state = @state.flatten
    for line in @@win_states do
      if linear_state[line[0]] == linear_state[line[1]] &&
        linear_state[line[0]] == linear_state[line[2]] &&
        linear_state[line[0]] != " "
        return linear_state[line[0]]
      end
    end
    nil
  end

  def is_full?
    @state.flatten.none? { |x| x == " " }
  end


end

=begin
x = Board.new
x.play_move("a1", "X")
x.play_move("a2", "X")
x.play_move("a3", "X")
x.play_move("b1", "X")
x.play_move("b2", "X")
x.play_move("b3", "X")
x.play_move("c1", "X")
x.play_move("c2", "X")
x.play_move("c3", "X")
puts x.is_full
=end
