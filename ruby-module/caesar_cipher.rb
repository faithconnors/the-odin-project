def caesar_cipher(str, n)
  arr = str.split("").map do |char|
    base = char.ord < 91 ? 65 : 97
    if char.ord.between?(65, 90) || char.ord.between?(97, 122)
      (((char.ord - base + n) % 26) + base).chr
    else
      char
    end
  end
  arr.join("")
end
