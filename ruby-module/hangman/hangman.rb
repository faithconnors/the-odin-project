require "json"

class Hangman

  @@letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  attr_reader :bad_guesses

  def initialize(word, guesses=Array.new, bad_guesses=0)
    @word = word.upcase.split("")
    @bad_guesses = bad_guesses;
    @guesses = guesses
  end

  def guess(letter)
    letter = letter.upcase

    unless @@letters.include?(letter) and letter.length == 1
      puts "\n\n"
      puts "\e[33mBad guess format! Remember, A-Z and only one letter!"
      puts "(This will not contribute to your guesses.)\e[0m"
      return false
    end

    if @guesses.include?(letter)
      puts "\n\n"
      puts "\e[33mDuplicate guess!"
      puts "(This will not contribute to your guesses.)\e[0m"
      return false
    end

    @guesses.push(letter)

    unless @word.include?(letter)
      @bad_guesses += 1
    end

    return true
  end

  def print_word()
    @word.each do |letter|
      print (@guesses.include?(letter)) ? letter : "_"
    end
    print "\n"
  end

  def print_guesses()
    puts "You have guessed the following letters:"
    @guesses.each do |letter|
      if @word.include?(letter)
        print "\e[32m#{letter} \e[0m"
      else
        print "\e[31m#{letter} \e[0m"
      end
    end
    print "\n"
  end

  def is_solved?()
    @word.all? { |letter| @guesses.include?(letter)}
  end

  def serialize_JSON(*options)
    {
      "json_class" => self.class.name,
      "data" => {
        guesses: @guesses,
        bad_guesses: @bad_guesses,
        word: @word
      }
    }.to_json(*options)
  end
end
