require "./hangman.rb"
require "json"

def pick_word()
  word_array = File.read("words.txt").split
  word = word_array.sample
  until word.length.between?(5, 12) do
    word = word_array.sample
  end
  word
end

hm = nil

class String
  def is_int?
    self.to_i.to_s == self
  end
end

def load_file()
  hangman = nil
  while true do

    puts "Enter save slot (1-5)"
    slot = gets.chomp

    if slot.is_int?
      slot = slot.to_i
    else
      puts "Not a number."
      next
    end

    unless slot.between?(1, 5)
      puts "Number not between 1 and 5."
      next
    end

    if File.exist?("save_#{slot}")
      save_file = File.open("save_#{slot}")
      save_data = save_file.read
      save_file.close
      save_data = JSON.parse(save_data)
      hangman = Hangman.new(save_data["data"]["word"].join(""), save_data["data"]["guesses"], save_data["data"]["bad_guesses"])
      break
    else
      puts "File does not exist"
      next
    end

    puts "File loaded!"
    break
  end
  return hangman
end

def save_file(hm)
  puts "Enter save slot (1-5)"
  while true do
    slot = gets.chomp

    unless slot.is_int?
      puts "Enter a number."
      next
    end

    slot = slot.to_i

    unless slot.between?(1, 5)
      puts "Enter a number between 1 and 5"
      next
    end

    save = File.open("save_#{slot}", "w+")
    save.write(hm.serialize_JSON)
    save.close
    break
  end
  puts "File saved to slot #{slot} (save_#{slot})"
end

puts "[1] New game  [2] Load Game  [3] Quit"
while true do
  choice = gets.chomp
  if choice == "1"
    hm = Hangman.new(pick_word)
    break
  elsif choice == "2"
    hm = load_file
    break
  elsif choice == "3"
    exit
  else
    puts "Enter a valid choice."
    next
  end

end
until hm.is_solved? do
  puts "\n\n"
  hm.print_word
  puts "You have made #{hm.bad_guesses}/5 bad guesses."
  puts "Take a guess! or [1] View Guesses  [2] Save Game  [3] Quit"
  input = gets.chomp

  if input == "1"
    hm.print_guesses
    next
  elsif input == "2"
    save_file(hm)
    exit
  elsif input == "3"
    exit
  end

  if (hm.guess(input))
    puts "Guessing #{input}..."
  else
    next
  end

  if hm.bad_guesses > 5
    break
  end

end

if (hm.is_solved?)
  puts "\n\n"
  puts "Congratulations! You solved the word with #{5 - hm.bad_guesses} remaining bad guesses!"
  hm.print_word
else
  puts "\n\n"
  puts "Couldn't guess the word, and hung the man."
  puts "Too bad..."
end
