require "./lib/linked_list.rb"

ll = LinkedList.new('b')
ll.append('c')
ll.prepend('a')

puts "head: #{ll.head.value}"
puts "tail: #{ll.tail.value}"
puts "list: #{ll.to_s}"
puts "popped: #{ll.pop.value}"
puts "new list: #{ll.to_s}"
puts "contains c?: #{ll.contains?('c')}"
puts "appending c, d, f..."
ll.append('c')
ll.append('d')
ll.append('f')
puts "find c: #{ll.find('c').value}"
puts "find e: #{ll.find('e')}"
puts "element at index 3: #{ll.at_index(3).value}"
puts "inserting e at index 4..."
ll.insert_at('e', 4)
puts "new list: #{ll.to_s}"
puts "removing element 4 (e)..."
puts "#{ll.remove_at(4).value}"
puts "list: #{ll.to_s}"
