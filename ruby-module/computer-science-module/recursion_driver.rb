require "./lib/recursion.rb"

puts "#{fibs(-1)}"
puts "#{fibs(0)}"
puts "#{fibs(1)}"
puts "#{fibs(2)}"
puts "#{fibs(8)}"

puts "#{fibs_rec(-1)}"
puts "#{fibs_rec(0)}"
puts "#{fibs_rec(1)}"
puts "#{fibs_rec(2)}"
puts "#{fibs_rec(8)}"

puts "MERGESORT TEST"

arr_to_sort = [5, 1, 2, 4, 6, 3]
arr_sorted = merge_sort(arr_to_sort)
puts "#{arr_sorted}"