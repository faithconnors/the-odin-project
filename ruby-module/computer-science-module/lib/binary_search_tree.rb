require_relative "./recursion.rb"

class Node
  include Comparable
  attr_accessor :data
  attr_accessor :left
  attr_accessor :right

  def initialize(data=nil, left=nil, right=nil)
    @data = data
    @left = left
    @right = right
  end

  def value
    @data
  end

  def add_child(child_data)
    if child_data < @data
      unless @left
        @left = Node.new(child_data)
        return
      end
      @left.add_child(child_data)
      return
    else
      unless @right
        @right = Node.new(child_data)
        return
      end
      @right.add_child(child_data)
    end
  end

  def <=>(other)
    return nil if other == nil
    @data <=> other.value
  end
end

class Tree
  def initialize(data)
    @root = nil
    self.build_tree(data)
  end

  def root
    @root
  end

  def build_tree(data)
    return if data.length == 0

    data = merge_sort(data).uniq
    mid_point = data.length / 2
    @root = Node.new(data[mid_point])
    return if data.length == 1

    @root.left = build_side(data[0...mid_point])
    return if data.length == 2

    @root.right = build_side(data[mid_point + 1..-1])
    @root
  end

  def find(data, node=@root)
    case data <=> node.value
    when -1
      return nil unless node.left
      find(data, node.left)
    when 0
      return node
    when 1
      return nil unless node.right
      find(data, node.right)
    end
  end

  def insert(data)
    return if self.find(data)
    @root.add_child(data)
  end

  def delete(data)
    node_to_delete = find(data)
    return unless node_to_delete

    parent_node = find_parent(data)
    l_side = node_to_delete.left
    r_side = node_to_delete.right

    #Case 1: No children
    if l_side == nil && r_side == nil
      if parent_node.value < data
        parent_node.right = nil
        return
      else
        parent_node.left = nil
        return
      end
    end

    #Case 2: 1 child
    if l_side == nil
      if parent_node.value < data
        parent_node.right = r_side
        return
      else
        parent_node.left = r_side
        return
      end
    elsif r_side == nil
      if parent_node.value < data
        parent_node.right = l_side
        return
      else
        parent_node.left = l_side
        return
      end
    end

    #Case 3: 2 children
    replacement = r_side
    while replacement.left do
      replacement = replacement.left
    end
    temp = replacement.value
    delete(replacement.value)
    node_to_delete.data = temp
  end

  def level_order
    queue = [@root]
    until queue.length == 0 do
      node = queue.shift
      yield node if block_given?

      if node.left
        queue << node.left
      end

      if node.right
        queue << node.right
      end
    end
  end

  def preorder(node=@root, arr=[], &block)
    if block_given?
      yield node
    else
      arr << node.value
    end

    if node.left
      arr = preorder(node.left, arr, &block)
    end
    if node.right
      arr = preorder(node.right, arr, &block)
    end
    arr
  end

  def inorder(node=@root, arr=[], &block)
    if node.left
      arr = inorder(node.left, arr, &block)
    end
    if block_given?
      yield node
    else
      arr << node.value
    end
    if node.right
      arr = inorder(node.right, arr, &block)
    end
    arr
  end

  def postorder(node=@root, arr=[], &block)
    if node.left
      arr = postorder(node.left, arr, &block)
    end
    if node.right
      arr = postorder(node.right, arr, &block)
    end
    if block_given?
      yield node
    else
      arr << node.value
    end
    arr
  end

  def rebalance
    self.build_tree(self.inorder)
  end

  def depth(node, start=@root, n=0)
    if node == nil
      return nil
    end
    case node <=> start
    when -1
      return depth(node, start.left, n+1)
    when 0
      return n
    when 1
      return depth(node, start.right, n+1)
    end
  end

  def height(node)
    return -1 if node == nil
    if node.left == nil && node.right == nil
      return 0
    end
    left_height = height(node.left) + 1
    right_height = height(node.right) + 1

    return (left_height > right_height) ? left_height : right_height
  end

  def balanced?(node=@root)
    return true if node == nil
    r_height = height(node.right)
    l_height = height(node.left)
    ordered = [r_height, l_height].minmax
    if ordered[1] - ordered[0] > 1
      return false
    end
    if balanced?(node.left) && balanced?(node.right)
      return true
    else
      return false
    end
  end

  #Copied from the official project guidelines
  def pretty_print(node = @root, prefix = '', is_left = true)
    pretty_print(node.right, "#{prefix}#{is_left ? '│   ' : '    '}", false) if node.right
    puts "#{prefix}#{is_left ? '└── ' : '┌── '}#{node.data}"
    pretty_print(node.left, "#{prefix}#{is_left ? '    ' : '│   '}", true) if node.left
  end

  private

  def build_side(data)
    mid_point = data.length / 2
    local_root = Node.new(data[mid_point])
    return local_root if data.length == 1
    local_root.left = build_side(data[0...mid_point])
    return local_root if data.length == 2
    local_root.right = build_side(data[mid_point + 1..-1])
    local_root
  end

  def find_parent(data, node=@root, parent=nil)
    case data <=> node.value
    when -1
      return nil unless node.left
      find_parent(data, node.left, node)
    when 0
      return parent
    when 1
      return nil unless node.right
      find_parent(data, node.right, node)
    end
  end

end

