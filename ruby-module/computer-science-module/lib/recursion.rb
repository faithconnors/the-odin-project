def fibs(n)
  return [] unless n > 0
  return [0] unless n != 1
  seq = [0, 1]
  return seq unless n != 2
  last = seq.length - 1
  (n-2).times do |i|
    seq << seq[last] + seq[last - 1]
    last += 1
  end
  seq
end

def fibs_rec(n)
  return [0] if n <= 1
  return [0, 1] if n == 2
  last_terms = fibs_rec(n - 1)
  last_terms << last_terms[n - 2] + last_terms[n - 3]
end

def merge_sort(arr)
  #sorted single
  return arr if arr.length == 1

  #dividing
  len = arr.length
  mid = len / 2
  arr_left = merge_sort(arr[0..mid-1])
  arr_right = merge_sort(arr[mid..len - 1])

  #merging
  last_left = arr_left.length - 1
  last_right = arr_right.length - 1
  index_left = 0
  index_right = 0
  merged_arrays = []

  while (index_left <= last_left && index_right <= last_right) do
    if (arr_left[index_left] < arr_right[index_right]) 
      merged_arrays << arr_left[index_left]
      index_left += 1
    else
      merged_arrays << arr_right[index_right]
      index_right += 1
    end
  end

  if (index_left > last_left) 
    for n in arr_right[index_right..last_right]
      merged_arrays << n
    end
  else
    for n in arr_left[index_left..last_left]
      merged_arrays << n
    end
  end
  merged_arrays
end