class MoveNode

  attr_reader :parent, :position

  @@history = []

  TRANSFORMS = [[1, 2], [-1, 2], [1, -2], [-1, -2], [2, 1], [2, -1], [-2, 1], [-2, -1]]

  def initialize(position, parent)
    @position = position
    @parent = parent
    @@history << position
  end

  def children
    TRANSFORMS.map { |t| [@position[0] + t[0], @position[1] + t[1]]}
      .select { |pos| (MoveNode.valid?(pos)) }
      .reject { |pos| @@history.include?(pos) }
      .map { |pos| MoveNode.new(pos, self)}
  end

  def self.valid?(pos)
    pos[0].between?(0, 7) && pos[1].between?(0, 7)
  end
end

def lineage(node)
  lineage(node.parent) unless node.parent == nil
  p node.position
end

def knights_travails(start_pos, end_pos)
  queue = []
  current_node = MoveNode.new(start_pos, nil)
  until current_node.position == end_pos do
    current_node.children.each { |child| queue << child }
    current_node = queue.shift
  end
  puts "Found solution:"
  lineage(current_node)
end
