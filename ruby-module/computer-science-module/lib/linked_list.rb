class Node

  attr_accessor :pr
  attr_accessor :nx

  def initialize(data, pr=nil, nx=nil)
    @data = data
    @nx = nx
    @pr = pr
  end

  def value
    @data
  end

end

class LinkedList
  def initialize(data)
    @head = Node.new(data)
    @tail = @head
    @size = 1
  end

  def append(data)
    @tail.nx = Node.new(data, @tail)
    @tail = @tail.nx
    if @head == nil
      @head = @tail
    end
    @size += 1
  end

  def prepend(data)
    @head.pr = Node.new(data, nil, @head)
    @head = @head.pr
    if @tail == nil
      @tail = @head
    end
    @size += 1
  end

  def head
    @head
  end

  def tail
    @tail
  end

  def find(data)
    current = @head
    while current do
      if current.value == data
        return current
      else
        current = current.nx
      end
    end
    nil
  end

  def at_index(n)
    return nil if @size <= n
    current = @head
    n.times do
      current = current.nx
    end
    current
  end

  def pop
    return nil if @size < 1
    to_return = @tail
    @tail = @tail.pr
    @size -= 1
    to_return
  end

  def contains?(data)
    current = @head
    while current do
      if current.value == data
        return true
      end
      current = current.nx
    end
    false
  end

  def to_s
    current = @head
    str = ""
    while current do
      str += "(#{current.value})"
      if current.nx
        str += " -> "
      end
      current = current.nx
    end
    str
  end

  def insert_at(data, n)
    return if @size < n || n < 0

    if n == 0
      self.prepend(data)
      return
    end

    if n == @size
      self.append(data)
      return
    end

    next_node = self.at_index(n)
    prev_node = next_node.pr

    new_node = Node.new(data, prev_node, next_node)
    next_node.pr = new_node
    prev_node.nx = new_node
    @size += 1
  end

  def remove_at(n)
    return nil if n < 0 || n >= @size

    if n == @size - 1
      return_node = self.pop
      return return_node
    end

    to_remove = self.at_index(n)
    next_node = to_remove.nx
    prev_node = to_remove.pr

    next_node.pr = prev_node
    prev_node.nx = next_node
    @size -= 1

    to_remove
  end

end
