require "./lib/binary_search_tree.rb"

tr = Tree.new(Array((0..50)))
tr.pretty_print

puts "Preorder test:"
tr.preorder { |node| print "#{node.value} " }
print "\n"

puts "Inorder test:"
tr.inorder { |node| print "#{node.value} " }
print "\n"

puts "Postorder test:"
tr.postorder { |node| print "#{node.value} " }
print "\n"

puts "Level order test:"
tr.level_order { |node| print "#{node.value} "}
print "\n"

puts "Inorder array test:"
puts "#{tr.inorder}"

tr.delete(45)
puts "\n\n"
tr.pretty_print

puts ("Depth of 2:")
puts "#{tr.depth(tr.find(2))}"

puts "Height of 3:"
puts "#{tr.height(tr.find(3))}"

puts "Height of 0:"
puts "#{tr.height(tr.find(0))}"

puts "Balanced test with smaller tree:"
tr_two = Tree.new(Array((0..7)))
tr_two.pretty_print
puts "#{tr_two.balanced?}"

puts "unbalancing small test..."
10.times do
  tr_two.insert(rand(100..200))
end

tr_two.pretty_print
puts "#{tr_two.balanced?}"

tr_two.rebalance
tr_two.pretty_print
