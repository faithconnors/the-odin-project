const add = (a, b) => a + b;
const sub = (a, b) => a - b;
const mul = (a, b) => a * b;
const div = (a, b) => (b != 0) ? a / b : "ERR";

const operators = ['+', '-', '*', '/'];

const performOperation = function(a, b, op) {
    switch (op) {
        case "+":
            return add(a, b);
        case "-":
            return sub(a, b);
        case "*":
            return mul(a, b);
        case "/":
            return div(a, b);
        default:
            return "UNKNOWN OPERATOR";
    }
}

const operate = function(str) {
    let tokens = Array.from(str)
        .map((char) => (operators.includes(char)) ? ` ${char} ` : char)
        .join("")
        .split(" ")
        .map((token) => isNaN(token) ? token : parseFloat(token));
        let tokenIndex = tokens.findIndex((token) => token == '/' || token == '*');
    while (tokenIndex != -1) {
        tokens[tokenIndex] = performOperation(tokens[tokenIndex - 1], tokens[tokenIndex + 1], tokens[tokenIndex]);
        tokens.splice(tokenIndex + 1, 1);
        tokens.splice(tokenIndex - 1, 1);
        tokenIndex = tokens.findIndex((token) => token == '/' || token == '*');
    }
    tokenIndex = tokens.findIndex((token) => token == '-' || token == '+');
    while (tokenIndex != -1) {
        tokens[tokenIndex] = performOperation(tokens[tokenIndex - 1], tokens[tokenIndex + 1], tokens[tokenIndex]);
        tokens.splice(tokenIndex + 1, 1);
        tokens.splice(tokenIndex - 1, 1);
        tokenIndex = tokens.findIndex((token) => token == '-' || token == '+');
    }
    console.log(tokens);
    return tokens[0];
}

const linearOperate = function(str) {
    const operatorsInString = Array.from(str).filter((char) => operators.includes(char));
    const operands = Array.from(str)
        .map((char) => (operators.includes(char)) ? " " : char)
        .join("")
        .split(" ")
        .map((n) => parseFloat(n));

    while (operands.length > 1) {
        operands[0] = performOperation(operands[0], operands[1], operatorsInString[0]);
        operands.splice(1, 1);
        operatorsInString.splice(0, 1);
    }
    return operands[0];
}

let expression = "";
const currentInput = document.querySelector(".current-input");
const formula = document.querySelector(".formula");
const ops = document.querySelectorAll(".oper");
const numbers = document.querySelectorAll(".number");

document.querySelector(".dec").addEventListener("mousedown", (e) => {
    if (!Array.from(currentInput.innerHTML).some((char) => char == '.')) {
        currentInput.innerHTML += '.';
    }
});

document.querySelector(".evaluate").addEventListener("mousedown", (e) => {
    expression += currentInput.innerHTML;

    console.log(expression);
    if (operators.includes(expression.charAt(expression.length - 1))) {
        expression.slice(0, expression.length - 1);
    }
    formula.innerHTML = expression + "=";
    currentInput.innerHTML = operate(expression);
    expression = "";
});

document.querySelector(".clear-all").addEventListener("mousedown", () => {
    expression = "";
    formula.innerHTML = expression;
    currentInput.innerHTML = "";
});

document.querySelector(".clear-input").addEventListener("mousedown", () => currentInput.innerHTML = "");

document.querySelector(".backspace").addEventListener("mousedown", () => currentInput.innerHTML = (currentInput.innerHTML.length > 0) ? currentInput.innerHTML.slice(0, currentInput.innerHTML.length - 1) : "");

numbers.forEach((node) => node.addEventListener("mousedown", (e) => {

    if (currentInput.innerHTML == "0") {
        if (!(e.target.dataset.key == "0")) {
            currentInput.innerHTML = e.target.dataset.key;
        }
        return;
    }
    currentInput.innerHTML += e.target.dataset.key;
}));

ops.forEach((node) => node.addEventListener("mousedown", (e) => {
    const currentNumber = currentInput.innerHTML;
    if (currentNumber != "" || currentNumber != '.') {
        if (currentNumber.charAt(currentNumber.length - 1) == '.') {
            currentNumber.slice(0, currentNumber.length - 1);
        }
        expression += currentNumber + e.target.dataset.key;
        formula.innerHTML = expression;
        currentInput.innerHTML = "0";
    }
}));

