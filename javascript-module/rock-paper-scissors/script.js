const score = document.querySelector(".score");
const result = document.querySelector(".description");
const round = document.querySelector(".round");

let currentRound = 1;
let playerscore = 0;
let cpuscore = 0;

const updateHTML = function(resultstr) {
    round.innerHTML = ("Round: " + currentRound);
    score.innerHTML = ("Player: " + playerscore + "  --  CPU: " + cpuscore);
    result.innerHTML = resultstr;
}

const sleep = function(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const getPlayerChoice = function() {
    let answer = prompt("Choose: 'rock', 'paper', or 'scissors'.").toLowerCase();
    switch (answer) {
        case "rock":
            return 0;
        case "paper":
            return 1;
        case "scissors":
            return 2;
        default:
            console.log("Select a valid option from the list.");
    }
}

const getCPUChoice = function() {
    return Math.floor(Math.random() * 3);
}

const playRound = async function(playerselection) {
    let computerselection = getCPUChoice();

    console.log("You played " + playerselection + 
        ". The CPU played " + computerselection + ".");

    if (playerselection == computerselection) {
        updateHTML("Draw");
    }
    else if (((playerselection + 1) % 3) == computerselection) {
        cpuscore++;
        currentRound++
        updateHTML("CPU wins.");
    }
    else {
        playerscore++;
        currentRound++;
        updateHTML("Player Wins.");
    }

    await sleep(100);

    if (playerscore == 5 || cpuscore == 5) {
        score.innerHTML = ("Player: " + playerscore + "  --  CPU: " + cpuscore);
        alert((playerscore > cpuscore) ? "Player wins!" : "CPU wins!");
        currentRound = 1;
        playerscore = 0;
        cpuscore = 0;
        updateHTML("Start a new game.");
    }
}

const paperButton = document.querySelector(".paper-button");
const rockButton = document.querySelector(".rock-button");
const scissorsButton = document.querySelector(".scissors-button");

paperButton.addEventListener("click", (e) => {
    let key = e.target.dataset.key;
    playRound(key);
});
rockButton.addEventListener("click", (e) => {
    let key = e.target.dataset.key;
    playRound(key);
});
scissorsButton.addEventListener("click", (e) => {
    let key = e.target.dataset.key;
    playRound(key);
});

