//Set up click-and-drag
window.mouseDown = false;
window.addEventListener("mousedown", () => window.mouseDown = true);
window.addEventListener("mouseup", () => window.mouseDown = false);

const createPixel = function(n) {
    let div = document.createElement('div');
    div.style.width = n + "px";
    div.style.height = n + "px";
    div.className = "pixel";
    div.addEventListener("mouseover", (e) => {
        if (window.mouseDown) {
            e.target.classList.add("transition");
        }
    });
    div.addEventListener("mousedown", (e) => {
        e.target.classList.add("transition");
    });
    return div;
}

const populateGrid = function(n) {
    let pixelSize = Math.floor(960 / n);
    for (let i = 0; i < n * n; ++i) {
        grid.appendChild(createPixel(pixelSize));
    }
}

const clearGrid = function() {
    let first = grid.firstChild;
    console.log(first);
    while (first) {
        grid.removeChild(first);
        first = grid.firstChild;
    }
}

const eraseAll = function() {
    let pixels = grid.childNodes;
    pixels.forEach((pixel) => pixel.classList.remove("transition"));
}

const grid = document.querySelector('.content');

const eraseButton = document.querySelector(".erase-button");
eraseButton.addEventListener("click", () => eraseAll());

const sliderSize = document.querySelector(".grid-size");
const sliderText = document.querySelector("h6");
sliderSize.addEventListener("input", 
    () => sliderText.innerHTML = "Size: " + sliderSize.value + "x" + sliderSize.value);

const setButton = document.querySelector(".grid-set");
setButton.addEventListener("click", () => {
    clearGrid();
    populateGrid(sliderSize.value);
});

populateGrid(16);