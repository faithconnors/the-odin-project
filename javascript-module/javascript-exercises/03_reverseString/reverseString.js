const reverseString = function(str) {
    let char_array = [];
    for (let i = str.length - 1; i >= 0; --i) {
        char_array.push(str.charAt(i));
    }
    return char_array.join("");
};

// Do not edit below this line
module.exports = reverseString;
