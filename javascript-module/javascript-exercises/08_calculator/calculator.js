const add = function(a, b) {
	return a + b;
};

const subtract = function(a, b) {
	return a - b;
};

const sum = function(a) {
  return a.reduce((total, y) => total + y, 0);
};

const multiply = function(a) {
  return a.reduce((product, y) => product * y, 1);
};

const power = function(a, b) {
	let product = 1;
  for (let i = 0; i < b; ++i) {
    product *= a;
  }
  return product;
};

const factorial = function(a) {
  if (a == 1) {
    return 1;
  }
  if (a == 0) {
    return 1;
  }
	return (a * factorial(a - 1));
};

// Do not edit below this line
module.exports = {
  add,
  subtract,
  sum,
  multiply,
  power,
  factorial
};
