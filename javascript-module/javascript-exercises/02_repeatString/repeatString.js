const repeatString = function(str, n) {
    if (n < 0) {
        return "ERROR";
    }
    let ret_str = ""
    for (let i = 0; i < n; ++i) {
        ret_str += str;
    }
    return ret_str;
};

// Do not edit below this line
module.exports = repeatString;
