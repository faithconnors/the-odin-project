const findTheOldest = function(arr) {
    const currentYear = (new Date).getFullYear();
    const oldest = arr.reduce((eldest, person) => {
        const age = (person.hasOwnProperty("yearOfDeath")) ? person.yearOfDeath - person.yearOfBirth : currentYear - person.yearOfBirth;
        eldest = (age > eldest.a) ? {p: person, a: age} : eldest;
        return eldest;
    }, {p: {}, a: 0});
    return oldest.p;
};

// Do not edit below this line
module.exports = findTheOldest;
