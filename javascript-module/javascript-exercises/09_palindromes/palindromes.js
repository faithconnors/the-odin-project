const punctuations = [',', '.', '!', '?', ' '];

const palindromes = function (str) {
    const filtered = Array.from(str).filter((char) => {
        return !punctuations.includes(char)
    }).join("");
    return filtered.toLowerCase() == filtered.split("").reverse().join("").toLowerCase();
};

// Do not edit below this line
module.exports = palindromes;
