const caesar = function(str, n) {
    if (n > 25 || n < -25) {
        n = n % 26;
    }
    const punct = [',','.','?','!',' ']
    const rotated = Array.from(str).map((char) => {
        if (punct.includes(char)) return char;
        const isLower = char == char.toLowerCase();

        char = char.charCodeAt(0) + n;
        if (isLower) {
            if (char > 122) return String.fromCharCode(char - 26);
            if (char < 97) return String.fromCharCode(char + 26);
            return String.fromCharCode(char);
        } 
        if (char > 90) return String.fromCharCode(char - 26);
        if (char < 65) return String.fromCharCode(char + 26);
        return String.fromCharCode(char);
    });
    return rotated.join("");
};

// Do not edit below this line
module.exports = caesar;
